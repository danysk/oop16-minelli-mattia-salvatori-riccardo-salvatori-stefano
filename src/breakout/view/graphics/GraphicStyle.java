package breakout.view.graphics;

import javafx.scene.text.Font;

/**
 * 
 *
 */
public interface GraphicStyle {

    /**
     * The font of this style.
     * 
     * @param size
     *            the size of the font
     * @return the font to use in the scene;
     */
    Font getFont(final double size);

    /**
     * Text style.
     * 
     * @return the ID of the text style in the css (see {@link #getCSS()})
     */
    String getTextStyle();

    /**
     * Main Pane style.
     * 
     * @return the ID of the main pane style in the css (see {@link #getCSS()})
     */
    String mainPaneStyle();

    /**
     * The style of the container with the status of the player.
     * 
     * @return the ID style in the css of the top pane that contains player
     *         information during the game (see {@link #getCSS()})
     */
    String statsContainerStyle();

    /**
     * CSS.
     * 
     * @return the css file that contains all the style IDs
     */
    String getCSS();

}

package breakout.controller;

import java.util.List;

/**
 * Functional interface to resolve inputs for user.
 *
 */
interface InputHandler {

    /**
     * Resolves an input producing the specific action.
     * 
     * @param input
     */
    void resolveInput(final Input input);

    /**
     * Resolve all the inputs in the given list.
     * 
     * @param inputList
     *            all the inputs to be resolved
     */
    default void resolveAll(final List<Input> inputList) {
        inputList.forEach(input -> this.resolveInput(input));
    }

}

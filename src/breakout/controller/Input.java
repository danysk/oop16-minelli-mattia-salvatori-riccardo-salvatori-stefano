package breakout.controller;

/**
 *
 */
public enum Input {

    /**
     * Move the paddle left.
     */
    LEFT,

    /**
     * Move the paddle right.
     */
    RIGHT,

    /**
     * Pause the game.
     */
    PAUSE,

    /**
     * Start the game.
     */
    START;

}
